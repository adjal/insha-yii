<?php

namespace app\components;

use yii\base\Widget;
use app\models\News;

class LastNewsWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
        $this->message = News::find()->limit(1)->orderBy('id DESC')->all();
    }

    public function run()
    {
        return $this->render('lastNews', [
            'messages' => $this->message,
        ]);
    }
}