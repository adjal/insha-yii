<?php

namespace app\components;

use yii\base\Widget;
use app\models\Comments;

class LastCommentsWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
        $this->message = Comments::find()->with('user')->orderBy('id DESC')->limit(4)->all();
    }

    public function run()
    {
        return $this->render('lastComments', [
            'messages' => $this->message,
        ]);

    }
}