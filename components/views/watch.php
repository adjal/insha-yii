<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="row">
        <div class="right_box">
            <div class="title">Иң укылганнары</div>
            <?php foreach ($messages as $message): ?>
                <div class="list">
                    <a  href="<?= Url::toRoute(['/text/view', 'id' => $message->id]) ?>" class="sub_title">
                        <?= Html::encode($message->title) ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>