<?php

use yii\helpers\StringHelper;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="row">
        <div class="right_box">
            <div class="title">Соңгы яңалыклар</div>
            <?php foreach ($messages as $message): ?>
                <div class="info_news">
                    <a  href="<?= Url::toRoute(['/news/view', 'id' => $message->id]) ?>" class="sub_title"><?= Html::encode($message->title) ?></a>
                    <div class="date"><?= Yii::$app->formatter->asDate($message->date) ?></div>
                </div>
                <div class="text">
                    <?= StringHelper::truncate(HtmlPurifier::process($message->text), 100, '...') ?>
                    <a href="<?= Url::toRoute(['/news/view', 'id' => $message->id]) ?>" class="link">укырга</a>
                </div>
            <?php endforeach; ?>
            <div class="all_news">
                <a href="<?= Url::toRoute(['/news/']) ?>">[барлык яңалыклар]</a>
            </div>
        </div>
    </div>
</div>