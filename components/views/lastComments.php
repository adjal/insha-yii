<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="row">
        <div class="right_box">
            <div class="title">Соңгы фикерләр</div>
            <?php foreach ($messages as $message): ?>
                <div class="last_comments">
                    <div class="user"> <?= $message->user['username'] ?? 'Аноним' ?> </div>
                    <?= Html::encode($message->text) ?>
                    <a  href="<?= Url::toRoute(['/text/view', 'id' => $message->text_id]) ?>#comments<?= $message->id ?>" class="sub_title"> укырга</a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>