<?php

namespace app\components;

use yii\base\Widget;
use app\models\Text;

class WatchWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
        $this->message = Text::find()->limit(5)->orderBy('watch DESC')->all();
    }

    public function run()
    {
        return $this->render('watch', [
            'messages' => $this->message,
        ]);

    }
}