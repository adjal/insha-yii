<?php

namespace app\components;

use app\models\Comments;
use yii\base\Widget;
use app\models\Text;

class StatisticsWidget extends Widget
{
    public $text;
    public $comments;

    public function init()
    {
        parent::init();
        $this->text = Text::find()->where(['moderation' => Text::STATUS_PUBLISH])->count();
        $this->comments = Comments::find()->count();
    }

    public function run()
    {
        return $this->render('statistics', [
            'text' => $this->text,
            'comments' => $this->comments,
        ]);

    }
}