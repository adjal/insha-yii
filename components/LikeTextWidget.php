<?php

namespace app\components;

use yii\base\Widget;
use app\models\Text;

class LikeTextWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
        $this->message = Text::find()->limit(5)->orderBy('rating DESC')->all();
    }

    public function run()
    {
        return $this->render('likeText', [
            'messages' => $this->message,
        ]);

    }
}