<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "vote_text".
 *
 * @property integer $id
 * @property integer $text_id
 * @property integer $user_id
 * @property integer $created_at
 */
class VoteText extends \yii\db\ActiveRecord
{

    /**
     * Behaviors
     */
    public function behaviors()
    {
        return [

            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ]

        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vote_text';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text_id', 'user_id'], 'required'],
            [['text_id', 'user_id', 'created_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text_id' => 'Text ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }
}
