<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "watch".
 *
 * @property integer $id
 * @property integer $text_id
 * @property integer $date
 * @property integer $ip
 * @property string $user_agent
 *
 * @property Text $text
 */
class Watch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'watch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text_id', 'date', 'ip', 'user_agent'], 'required'],
            [['text_id', 'date', 'ip'], 'integer'],
            [['user_agent'], 'string', 'max' => 255],
            [['text_id'], 'exist', 'skipOnError' => true, 'targetClass' => Text::className(), 'targetAttribute' => ['text_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text_id' => 'Text ID',
            'date' => 'Date',
            'ip' => 'Ip',
            'user_agent' => 'User Agent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getText()
    {
        return $this->hasOne(Text::className(), ['id' => 'text_id']);
    }
}
