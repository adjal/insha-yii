<?php

namespace app\models;

use app\modules\user\models\User;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property integer $text_id
 * @property integer $date
 * @property string $text
 * @property integer $ip
 * @property string $user_agent
 * @property integer $user_id
 *
 * @property Text $text0
 * @property User $user
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text_id', 'date',], 'required'],
            [['text'], 'required', 'message' => 'Фикерегезне өстәгез'],
            [['text_id', 'date', 'ip', 'user_id'], 'integer'],
            [['text'], 'string'],
            [['user_agent'], 'string', 'max' => 150],
            [['text_id'], 'exist', 'skipOnError' => true, 'targetClass' => Text::className(), 'targetAttribute' => ['text_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text_id' => 'Text ID',
            'date' => 'Date',
            'text' => 'Сезнең фикер',
            'ip' => 'Ip',
            'user_agent' => 'User Agent',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getText0()
    {
        return $this->hasOne(Text::className(), ['id' => 'text_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
