<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "text".
 *
 * @property integer $id
 * @property integer $date
 * @property integer $rating
 * @property string $title
 * @property string $text
 * @property integer $moderation
 * @property integer $ip
 * @property integer $watch
 * @property integer $section_id
 * @property integer $author_id
 */
class Text extends \yii\db\ActiveRecord
{
    /**
     * Publish status
     */
    const STATUS_PUBLISH = 1;

    /**
     * Unpublish status
     */
    const STATUS_UNPUBLISH = 0;

    /**
     * Default section id
     */
    const DEFAULT_SECTION = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'text';
    }

    /**
     * Behaviors
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['moderation', 'default', 'value' => self::STATUS_UNPUBLISH],
            ['section_id', 'default', 'value' => self::DEFAULT_SECTION],
            [['moderation', 'ip', 'section_id'], 'required'],
            [['date', 'rating', 'moderation', 'ip', 'watch', 'section_id', 'author_id'], 'integer'],
            ['title', 'required', 'message' => 'Исемне языгыз'],
            ['text', 'required', 'message' => 'Текстны языгыз'],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['section_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'rating' => 'Rating',
            'title' => 'Исем',
            'text' => 'Текст',
            'moderation' => 'Moderation',
            'ip' => 'Ip',
            'watch' => 'Watch',
            'section_id' => 'Бүлек',
            'author_id' => 'Author ID',
        ];
    }

    public function getCommentsCount()
    {
        return Comments::find()->where(['text_id' => $this->id])->count();
    }
}
