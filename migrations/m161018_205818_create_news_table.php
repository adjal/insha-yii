<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m161018_205818_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'date' => $this->integer(11),
            'title' => $this->string(150),
            'text' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
