<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comments`.
 */
class m161016_132424_create_comments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('comments', [
            'id' => $this->primaryKey(),
            'text_id' => $this->integer(11)->notNull(),
            'date' => $this->integer(11)->notNull(),
            'text' => $this->text(),
            'ip' => $this->bigInteger(14),
            'user_agent' => $this->string(150),
        ]);

        $this->addForeignKey(
            'fk-comments-text_id',
            'comments',
            'text_id',
            'text',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-comments-text_id',
            'comments'
        )
        ;
        $this->dropTable('comments');
    }
}
