<?php

use yii\db\Migration;

/**
 * Handles the creation of table `text`.
 */
class m161013_181217_create_text_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('text', [
            'id' => $this->primaryKey(),
            'date' => $this->integer(11)->notNull(),
            'rating' => $this->integer(7),
            'title' => $this->string(255)->notNull(),
            'text' => $this->text()->notNull(),
            'moderation' => $this->smallInteger(6)->notNull(),
            'ip' => $this->bigInteger(14)->notNull(),
            'watch' => $this->bigInteger(14),
            'section_id' => $this->integer(2)->notNull(),
            'author_id' => $this->integer(11)->notNull(),
        ]);

        $this->addForeignKey(
            'fk-text-section_id',
            'text',
            'section_id',
            'section',
            'id'
        );

        $this->addForeignKey(
            'fk-text-author_id',
            'text',
            'author_id',
            'user',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-text-author_id',
            'text'
        );

        $this->dropForeignKey(
            'fk-text-section_id',
            'text'
        );

        $this->dropTable('text');
    }
}
