<?php

use yii\db\Migration;

class m170721_201716_create_vote_text_table extends Migration
{
    /**
     * Table name
     */
    const TABLE = 'vote_text';

    public function up()
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey(11),
            'text_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
        ]);
    }

    public function down()
    {
        echo "m170721_201716_create_vote_text_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
