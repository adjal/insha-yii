<?php

use yii\db\Migration;

/**
 * Handles adding user_id to table `comments`.
 */
class m170511_194411_add_user_id_column_to_comments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('comments', 'user_id', $this->integer());

        $this->addForeignKey(
            'fk-comments-user_id',
            'comments',
            'user_id',
            'user',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-comments-user_id',
            'comments'
        );

        $this->dropColumn('comments', 'user_id');
    }
}
