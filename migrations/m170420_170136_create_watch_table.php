<?php

use yii\db\Migration;

/**
 * Handles the creation of table `watch`.
 */
class m170420_170136_create_watch_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('watch', [
            'id' => $this->primaryKey(),
            'text_id' => $this->integer(11)->notNull(),
            'date' => $this->integer(11)->notNull(),
            'ip' => $this->bigInteger(14)->notNull(),
            'user_agent' => $this->string(255)->notNull()
        ]);

        $this->addForeignKey(
            'fk-watch-text_id',
            'watch',
            'text_id',
            'text',
            'id'
            );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-watch-text_id',
            'watch'
        );

        $this->dropTable('watch');
    }
}
