<?php

use yii\helpers\Html;

$this->title = $model->title . Yii::$app->name;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->title,
]);
?>

<div class="text">

    <h1><?= Html::encode($model->title) ?></h1>

<div class="date"><?= Yii::$app->formatter->asDate($model->date) ?></div>

<div class="view"><?= Html::encode($model->text) ?></div>


</div>