<?php
/* @var $this yii\web\View */

use yii\widgets\ListView;

$this->title = 'Яңалыклар' . Yii::$app->name;
?>

<h2>Яңалыклар</h2>

<?= ListView::widget([
    'dataProvider' => $news,
    'itemView' => '_list',
    'summary' => '',
    'pager' => [
        'firstPageLabel' => 'беренче',
        'lastPageLabel' => 'соңгы',
    ],

]);

