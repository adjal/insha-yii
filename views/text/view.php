<?php
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $modelComments app\models\Comments */
/* @var $form ActiveForm */
/* @var $comments app\models\Comments */

$this->title = $model->title . Yii::$app->name;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->title,
]);
?>

<div class="text">
    <h1><!-- <a href="/section/1/">Инша</a> / --><?= Html::encode($model->title) ?></h1>

    <div class="date"><?= Yii::$app->formatter->asDate($model->date) ?></div>

    <div class="view"><?= nl2br(Html::encode($model->text)) ?></div>

    <div class="block">
        Бу язма <b> <?= $model->rating ?> </b> кешегә булышты. Ә сиңа ?
        <a href="<?= Url::to(['text/vote', 'type' => 'rox', 'id' => $model->id]) ?>">булышты</a> |
        <a href="<?= Url::to(['text/vote', 'type' => 'sox', 'id' => $model->id]) ?>">булышмады</a><br>
        Язманы <b> <?= $model->watch ?> </b> кеше укыды</p>
    </div>

</div>

<div class="comments">
    <div class="name"><a name="comments"></a>фикерләр (<?= count($comments) ?>)</div>

    <?php if (empty($comments)): ?>
        <p>Кызганычка каршы бу бүлектә әлегә фикерләр юк</p>
    <?php else : ?>
        <?php foreach ($comments as $comment): ?>
            <div class="user"><a name="comments<?= $comment->id ?>"></a><?= $comment->user['username'] ?? 'Аноним' ?></div>
            <div class="date"><?= Yii::$app->formatter->asTime($comment->date) ?></div>
            <p><?= Html::encode($comment->text) ?></p>
        <?php endforeach; ?>
    <?php endif; ?>

   <?php if (\Yii::$app->user->isGuest): ?>
       <div class="comments-registration alert alert-info" role="alert">
           Бары тик <a href="<?= Url::to(['/user/user/signup']) ?>" class="alert-link">теркәлгән</a>
           һәм <a href="<?= Url::to(['/user/user/login']) ?>" class="alert-link">авторизация</a> үткән кулланучылар гына фикер калдыра ала.
       </div>
   <?php else: ?>

       <div class="views-comment">

           <?php $form = ActiveForm::begin(); ?>

           <?= $form->field($modelComments, 'text')->textarea(['rows' => 5]) ?>

           <div class="form-group">
               <?= Html::submitButton('Өстәү', ['class' => 'btn btn-primary']) ?>
           </div>
           <?php ActiveForm::end(); ?>

       </div><!-- views-comment -->

   <?php endif; ?>
</div>


