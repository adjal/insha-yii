<?php
/* @var $this yii\web\View */

use yii\widgets\ListView;

$this->title = 'Баш бит' . Yii::$app->name;
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Интернет челтәрендәге беренче инашалар сайты. Сез бездә иншалар, татарча сочинениялар, язмалар, юлъязмалар һ.б. таба аласыз. Рәхим итегез.',
]);

echo ListView::widget([
    'dataProvider' => $listDataProvider,
    'itemView' => '_list',
    'summary' => '',
    'pager' => [
        'firstPageLabel' => 'беренче',
        'lastPageLabel' => 'соңгы',
    ],

]);
?>