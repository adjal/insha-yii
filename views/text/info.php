<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $message string */
/* @var $title string */
/* @var $alert string type of alert: success, info, warning, danger */

$this->title = !empty($title) ? $title : $message;
$this->title .= Yii::$app->name;

?>
<div class="alert alert-<?= $alert ?>" role="alert"><?= $message ?></div>