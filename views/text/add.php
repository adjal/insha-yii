<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Text */
/* @var $form ActiveForm */

$this->title = 'Өстәү' . Yii::$app->name;
?>
<div class="add">
    <h2>Өстәү</h2>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'text')->textarea(['rows' => 20]) ?>

    <div class="form-group">
        <?= Html::submitButton('Өстәү', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- test -->