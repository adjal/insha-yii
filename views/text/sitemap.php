<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Барлык иншалар, сочинениялар' . Yii::$app->name;
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Барлык иншалар, сочинениялар бер биттә.',
]);

?>

<div class="text">
<h1>Иншалар, татарча сочинениялар</h1>

<?php foreach ($text as $value): ?>

    <div class="block">
    <a  href="<?= Url::toRoute(['text/view', 'id' => $value->id]) ?>" class="sub_title">
        <?= Html::encode($value->title) ?>
    </a>
    </div>

<?php endforeach; ?>
</div>