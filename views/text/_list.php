<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\StringHelper;
use yii\helpers\Url;

?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box">
    <div class="row">
        <a href="<?= Url::toRoute(['text/view', 'id' => $model->id]) ?>" class="title"><?= Html::encode($model->title) ?></a>
        <div class="date"><?= Yii::$app->formatter->asDate($model->date) ?></div>
        <div class="text"><?= StringHelper::truncate(HtmlPurifier::process($model->text), 600, '...') ?></div>
        <div class="items">
            <a href="<?= Url::toRoute(['text/view', 'id' => $model->id]) ?>#comments" class="comments">фикерләр (<?= $model->getCommentsCount() ?>)</a>
            <a href="<?= Url::toRoute(['text/view', 'id' => $model->id]) ?>" class="link">тулаем</a>
        </div>
    </div>
</div>