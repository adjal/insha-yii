<?php

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\widgets\Menu;
use app\components\LastCommentsWidget;
use app\components\LikeTextWidget;
use app\components\WatchWidget;
use app\components\StatisticsWidget;
use app\components\LastNewsWidget;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <?= Html::csrfMetaTags() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <?php $this->beginBody() ?>

    <!-- Yandex.Metrika counter -->
    <script>
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter26013825 = new Ya.Metrika({
                        id:26013825,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true,
                        trackHash:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/26013825" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-87489630-1', 'auto');
      ga('send', 'pageview');

    </script>

    <!-- header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <a href="/" class="logo pull-left"><img src="/img/logo.png" alt="logo"></a>
                    <div class="search pull-right">
                        <form>
                            <input type="text" name="text">
                            <button>Эзлэу</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header -->

    <!-- menu -->

    <?php

    $menuItems = [
        ['label' => 'Баш бит', 'url' => ['/text/index']],
        ['label' => 'Сайт турында', 'url' => ['/text/about']]
    ];

    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Теркәлү', 'url' => ['/user/user/signup']];
        $menuItems[] = ['label' => 'Керү', 'url' => ['/user/user/login']];
    } else {
        /*$menuItems[] = ['label' => 'Заказ биру', 'url' => ['/text/order']];*/
        $menuItems[] = ['label' => 'Өстәү', 'url' => ['/text/add']];
        $menuItems[] = [
            'label' => 'Чыгу [' . Yii::$app->user->identity['username'] . ']',
            'url' => ['/user/user/logout'],
            'linkOptions' => ['data-method' => 'post'],
        ];
    }
    ?>

    <nav>
        <?= Menu::widget([
            'options' => ['class' => 'menu center-block hidden-xs'],
            'items' => $menuItems
        ]);?>
    </nav>
    <!-- menu -->

    <!-- content -->
    <div class="content">
        <div class="container bg">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?= $content ?>
                </div>

                <!-- sidebar -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 sidebar">

                    <!-- last news -->
                    <?= LastNewsWidget::widget() ?>
                    <!-- last news -->

                    <!-- read -->
                    <?= WatchWidget::widget() ?>
                    <!-- read -->

                    <!-- like -->
                    <?= LikeTextWidget::widget() ?>
                    <!-- like -->

                    <!-- last comments -->
                    <?= LastCommentsWidget::widget() ?>
                    <!-- last comments -->

                    <!-- statistics -->
                    <?= StatisticsWidget::widget() ?>
                    <!-- statistics -->

                </div>
                <!-- sidebar -->

            </div>
        </div>
    </div>
    <!-- content -->

    <footer>
        <div class="container">
            <div class="col-md-12">
                <div class="text-center">
                    ©2012-<?=date('Y');?> insha.ru Барлык мәгълүмәт интернет челтәреннән табылды.
                    <a  href="<?= Url::toRoute(['/text/sitemap']) ?>">Барлык иншалар.</a>
                </div>
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>