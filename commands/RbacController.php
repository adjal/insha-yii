<?php

namespace app\commands;

use yii\console\Controller;
use Yii;
use app\models\User;

class RbacController extends Controller
{

    /**
     * Generates roles
     */
    public function actionInit()
    {
        $auth = Yii::$app->getAuthManager();
        $auth->removeAll();

        $user = $auth->createRole('user');
        $auth->add($user);

        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $auth->addChild($admin, $user);

        $this->stdout('Done!' . PHP_EOL);
    }

    public function actionTest()
    {
        $user = new User(['id' => 1, 'username' => 'User']);

        Yii::$app->user->login($user);


    }
}