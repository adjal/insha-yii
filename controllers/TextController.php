<?php

namespace app\controllers;

use app\models\Section;
use Yii;
use app\models\Text;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use app\models\Comments;
use app\models\ContactForm;
use app\models\Watch;
use app\models\VoteText;
use yii\helpers\Url;

class TextController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Text::find()->where(['moderation' => Text::STATUS_PUBLISH])->orderBy('date DESC'),
            'pagination' => [
                'pageSize' => 10,
                'defaultPageSize' => 10,
            ],
        ]);
        return $this->render('index', [
            'listDataProvider' => $dataProvider
        ]);
    }

    public function actionView($id)
    {
        $model = Text::find()->where([
            'id' => $id,
            'moderation' => Text::STATUS_PUBLISH
        ])->one();

        if ($model === null) {
            throw new NotFoundHttpException;
        }

        // TODO Сервиска кучерергэ
        $model->updateCounters(['watch' => 1]);

        $modelComments = new Comments();
        $modelComments->text_id = $model->id;
        $modelComments->date = time();
        $modelComments->user_id = Yii::$app->user->getId();
        if ($modelComments->load(Yii::$app->request->post()) && $modelComments->save()) {
            return $this->refresh();
        }

        $allComments = Comments::find()->with('user')->where(['text_id' => $id])->all();

        return $this->render('view', [
            'model' => $model,
            'modelComments' => $modelComments,
            'comments' => $allComments,
        ]);
    }

    /**
     *
     * Add vote to text
     *
     * @param string $type
     * @param int $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionVote(string $type, int $id)
    {
        $user = Yii::$app->user->getId();

        if (Yii::$app->user->isGuest) {
            return $this->render('info', [
                'alert' => 'warning',
                'message' => 'Бары тик <a href=' . Url::to(['/user/user/signup']) . ' class="alert-link">теркәлгән</a>
                һәм <a href="' . Url::to(['/user/user/login']) . '" class="alert-link">авторизация</a> үткән кулланучылар
                 гына тавыш бирә ала.',
                'title' => 'Бары тик теркәлгән һәм авторизация үткән кулланучылар гына тавыш бирә ала',
            ]);
        }

        $model = Text::find()->where([
            'id' => $id,
            'moderation' => Text::STATUS_PUBLISH
        ])->one();

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        $vote = VoteText::find()->where([
            'text_id' => $id,
            'user_id' => $user,
        ])->one();


        if ($vote === null) {
            $newVote = new VoteText();
            $newVote->text_id = $id;
            $newVote->user_id = $user;
            $newVote->save();

            $update = (($type == 'rox') ? 1 : (($type == 'sox') ? '-1' : 0));
            $model->updateCounters(['rating' => $update]);
        } else {
            return $this->render('info', [
                'alert' => 'warning',
                'message' => 'Сез инде бу текстка тавыш бирдегез!'
            ]);
        }

        return $this->redirect(['text/view', 'id' => $id]);

    }

    public function actionOrder()
    {
        throw new NotFoundHttpException;
        return $this->render('order');
    }

    public function actionAdd()
    {
        $model = new Text();

        $sections = Section::find()->all();
        $sections = ArrayHelper::map($sections, 'id', 'name');

        $ip = ip2long(Yii::$app->getRequest()->getUserIP());
        $model->ip = $ip;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->render('info', [
                'alert' => 'success',
                'message' => 'Сезнең текст модерацияга жибәрелде. Модерация уткәч сайтта чыгыр. Рәхмәт!'
            ]);
        }

        return $this->render('add',[
            'model' => $model,
            'section' => $sections
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionSitemap()
    {
        return $this->render('sitemap', [
            'text' => Text::find()->where(['moderation' => Text::STATUS_PUBLISH])->all()
        ]);
    }

}
