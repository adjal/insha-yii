<?php

namespace app\controllers;

use app\models\News;
use yii\data\ActiveDataProvider;

class NewsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::find()->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 5,
                'defaultPageSize' => 5,
            ],
        ]);

        return $this->render('index',[
            'news' => $dataProvider
        ]);
    }

    public function actionView(int $id)
    {
        $news = News::find()->where(['id' => $id])->one();

        return $this->render('view', [
           'model' => $news
        ]);
    }

}
