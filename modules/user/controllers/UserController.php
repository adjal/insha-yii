<?php

namespace app\modules\user\controllers;

use app\modules\user\models\RegForm;
use app\modules\user\models\User;
use Yii;
use app\modules\user\models\LoginForm;

class UserController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSignup()
    {
        $model = new RegForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($user = $model->reg()) {
                if ($user->status === User::STATUS_ACTIVE) {
                    if (Yii::$app->getUser()->login($user)) {
                        return $this->goHome();
                    }
                }
            } else {
                Yii::$app->session->setFlash('error', 'Теркэлу вакытында хата чыкты.');
                Yii::error('Теркэлу вакытында хата');
                return $this->refresh();
            }
        }

        return $this->render('reg', [
            'model' => $model,
        ]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $errors = $model->errors;
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
        return $this->render(
            'login',
            [
                'model' => $model,
            ]
        );
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['/']);
    }

}
