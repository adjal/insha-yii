<?php

namespace app\modules\user\models;

use yii\base\Model;
use Yii;

class LoginForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $rememberMe = true;
    public $status;
    private $_user = false;

    public function rules()
    {
        return [
            [['email', 'password'], 'required', 'message' => 'Тутырылган булырга тиеш'],
            ['email', 'email'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword']
        ];
    }
    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Email яки серсүз дөрес түгел');
            }
        }
    }
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
    public function attributeLabels()
    {
        return [
            'username' => 'Исем',
            'email' => 'Email',
            'password' => 'Серсүз',
            'rememberMe' => 'Истә калдырырга'
        ];
    }
    public function login()
    {
        if ($this->validate()) {
            $this->status = ($user = $this->getUser()) ? $user->status : User::STATUS_INACTIVE;
            if ($this->status === User::STATUS_ACTIVE) {
                return Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
            } else {
                return false;
            }
        }

        return false;
    }
}