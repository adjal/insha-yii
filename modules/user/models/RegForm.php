<?php

namespace app\modules\user\models;

use yii\base\Model;

class RegForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $status;

    public function rules()
    {
        return [
            [['username', 'email', 'password'], 'filter', 'filter' => 'trim'],
            [['username', 'email', 'password'], 'required', 'message' => 'Тутырылган булырга тиеш'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['password', 'string', 'min' => 6, 'max' => 255],
            ['username', 'unique',
                'targetClass' => User::class,
                'message' => 'Бу исем инде кулланыла',
            ],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass' => User::class,
                'message' => 'Бу email инде кулланыла',
            ],
            ['status', 'default', 'value' => User::STATUS_ACTIVE, 'on' => 'default'],
            ['status', 'in', 'range' => [
                User::STATUS_INACTIVE,
                User::STATUS_ACTIVE,
            ]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Исем',
            'email' => 'Email',
            'password' => 'Серсүз',
        ];
    }

    public function reg()
    {
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->status = $this->status;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        return $user->save() ? $user : null;
    }
}